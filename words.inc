%include "colon.inc"

section .data
    colon "group", fourth_word
    db "P3233", 0

    colon "itmo", third_word
    db "university ITMO", 0

    colon "surname", second_word
    db "Khasanshin", 0

    colon "name", first_word
    db "Marat", 0

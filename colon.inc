%define start_adr 0x0	

%macro colon 2
	%2:
		dq start_adr
		db %1, 0
	%define start_adr %2
%endmacro

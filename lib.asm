section .text
global exit
global string_length
global print_string
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_newline_stderr

%define read_syscall 0
%define write_syscall 1
%define exit_syscall 60
%define null_terminate 0
%define stdin 0
%define stdout 1
%define stderr 2
%define newline_char 0xA
%define tab_char 0x9
%define space_char 0x20
%define true 1
%define false 0
%define zero_char 0x30
%define nine_char 0x39
%define minus_char 0x2D

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, exit_syscall
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .counter:
    cmp byte[rdi + rax], null_terminate
    je .end
    inc rax
    jmp .counter
    .end:
    ret


global print_error
; Принимает указатель на нуль-терминированную строку, выводит ее в stderr
print_error:
    push rdi
    call string_length
    pop rsi
    mov rdi, stderr
    mov rdx, rax
    mov rax, write_syscall
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdi, stdout
    mov rdx, rax
    mov rax, write_syscall
    syscall
    ret

print_newline_stderr:
    mov rdi, newline_char
    push rdi
    mov rsi, rsp
    mov rax, write_syscall
    mov rdi, stderr
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, newline_char

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, write_syscall
    mov rdi, stdout
    mov rdx, 1
    syscall
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    xor rdx, rdx
    mov rsi, rsp
    dec rsi
    mov rcx, 10
    push 0
    sub rsp, 16
    test rax, rax
    jz .print_uint_zero
    .print_uint_loop:
    test rax, rax
    jz .print_uint_end
    div rcx
    add rdx, zero_char
    dec rsi
    mov [rsi], dl
    xor rdx, rdx
    jmp .print_uint_loop
    .print_uint_zero:
    add rdx, zero_char
    dec rsi
    mov [rsi], dl
    .print_uint_end:
    mov rdi, rsi
    call print_string
    add rsp, 24
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    test rax, rax
    jns .print_int_positive
    .print_minus_char:
    mov rdi, minus_char
    push rax
    call print_char
    pop rax
    neg rax
    .print_int_positive:
    mov rdi, rax
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .string_equals_loop:
    mov al, [rsi]
    cmp al, [rdi]
    jnz .string_not_equal
    inc rsi
    inc rdi
    cmp al, null_terminate
    jnz .string_equals_loop
    mov rax, true
    ret

    .string_not_equal:
    mov rax, false
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
   push 0
   xor rax, rax
   xor rdi, rdi
   mov rsi, rsp
   mov rdx, 1
   syscall
   pop rax
   ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    xor rdx, rdx
    push rdi

    .read_word_skip_at_start:
    push rdx
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop rdx
    cmp al, space_char
    je .read_word_skip_at_start
    cmp al, tab_char
    je .read_word_skip_at_start
    cmp al, newline_char
    je .read_word_skip_at_start

    .read_word_loop:
    cmp rdx, rsi
    je .read_word_longer_than_buffer
    cmp al, space_char
    je .read_word_end
    cmp al, tab_char
    je .read_word_end
    cmp al, newline_char
    je .read_word_end
    cmp al, null_terminate
    je .read_word_end
    mov byte[rdi], al
    inc rdi
    inc rdx
    push rdx
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop rdx
    jmp .read_word_loop

    .read_word_end:
    mov byte[rdi], 0
    pop rax
    ret
    .read_word_longer_than_buffer:
    pop rax
    xor rax, rax
    mov rdx, 0
    ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rdx, rdx
	cmp byte [rdi], null_terminate
	je .parse_int_exit
.parse_int_loop:
	movzx rcx, byte [rdi]
	cmp rcx, zero_char
	jb .parse_int_exit
	cmp rcx, nine_char
	ja .parse_int_exit

	imul rax, rax, 10
	sub rcx, zero_char
	add rax, rcx

	inc rdi
	inc rdx

	jmp .parse_int_loop
.parse_int_exit:
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    jne .positive

    .negative:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret
    .positive:
        jmp parse_uint

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor ecx, ecx
    .string_copy_loop:
        cmp rcx, rdx
        je .buffer_overflow

        mov al, [rdi + rcx]
        mov [rsi + rcx], al

        inc ecx
        cmp byte [rdi + rcx - 1], 0
        jne .string_copy_loop

        mov eax, ecx
        ret
    .buffer_overflow:
        xor rax, rax
        ret

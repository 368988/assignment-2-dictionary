%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define BUFF_SIZE 256
%define PSIZE 8

section .bss
buffer: resb BUFF_SIZE

section .rodata
input_err: db "invalid input", 0
not_found_err: db "Key wasn't found", 0

section .text
global _start:
_start:
	mov rdi, buffer
	mov rsi, BUFF_SIZE
	call read_word
	test rdx, rdx
	jz .input_err

	mov rdi, buffer
	mov rsi, start_adr
	call find_word

	test rax, rax
	jz .find_err

	add rax, PSIZE
	push rax
	call string_length
	pop rdx
	add rax, rdx
	inc rax
	mov rdi, rax
	call print_string
	call print_newline
	xor rdi, rdi
	jmp exit

.input_err:
	mov rdi, input_err
	call print_error
	mov rdi, 1
	jmp exit
.find_err:
	mov rdi, not_found_err
	call print_error
	mov rdi, 1
	jmp exit

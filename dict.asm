%include "lib.inc"
global find_word

section .text
%define jump_key 8


find_word:
	.loop:
		mov rdx, rsi
		test rdx, rdx
		jz .notfound
		add rsi, jump_key
		push rdi
		call string_equals
		pop rdi
		cmp rax, 1
		je .found
		cmp qword[rdx], 0x0
		je .notfound
		mov rsi, [rdx]
		jmp .loop
	.notfound:
		xor rax, rax
		ret
	.found:
		mov rax, rdx
		ret
